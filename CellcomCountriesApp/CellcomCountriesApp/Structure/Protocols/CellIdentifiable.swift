//
//  CellIdentifiable.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import UIKit.UINib

protocol CellIdentifiable : NSObjectProtocol {
    static var cellID: String { get }
    static var cellNib: UINib  { get }
}

extension CellIdentifiable {
    static var cellID: String {
        return String(describing: Self.self)
    }
    
    static var cellNib: UINib {
        return UINib(nibName: cellID, bundle: nil)
    }
}
