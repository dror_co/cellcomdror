//
//  Observable.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 28/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

class Observable<T> {
    typealias Listener = (T) -> ()
    
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    deinit {
        listener = nil
    }
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(_ listener: Listener?) {
        bind(listener)
        listener?(value)
    }
}
