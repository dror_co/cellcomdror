//
//  Countries.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

struct Countries: Codable {
    var countries: [Country]?
}
