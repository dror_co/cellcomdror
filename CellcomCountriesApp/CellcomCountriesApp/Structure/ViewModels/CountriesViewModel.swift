//
//  CountriesViewModel.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 01/05/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

enum ViewModelType: String {
    case all, single
}

enum SortType: String {
    case name, area
}

enum SortDirectionType: String {
    case up, down
}

class CountriesViewModel {
    
    var countriesModel: Observable<Countries?>? = Observable(nil)
    var countryModel: Observable<Country?>? = Observable(nil)
    var sortTypeDirection: [SortType : SortDirectionType] = [.name : .down,
                                                             .area : .down]
    
    init(type: ViewModelType, country: Country? = nil) {
        if type == .all {
            sendCountriesRequest()
        }
        else if let country = country, type == .single {
            sendBorderCountriesRequest(country)
        }
    }
    
    func navigationBarLeftItemPressed() {
        sortByName()
    }
    
    func navigationBarRightItemPressed() {
        sortByArea()
    }
    
    private func sendCountriesRequest() {
        let urlString = NetworkManager.baseUrl + NetworkManager.suffixAllCountries
        if let url = URL(string: urlString) {
            NetworkManager.fetchData(url: url) { [weak self] result in
                switch result {
                case .success(let data):
                    do {
                        let value = try JSONDecoder().decode([Country].self, from: data)
                        if value.count > 0 {
                            self?.countriesModel?.value = Countries(countries: value)
                        }else{
                            self?.countriesModel?.value = Countries(countries: value)
                            print("No Countries Received")
                        }
                    } catch {
                        self?.countriesModel?.value = Countries(countries: [])
                        print(error.localizedDescription)
                    }
                case .failure:
                    self?.countriesModel?.value = Countries(countries: [])
                    print("Fail To Fetch Data")
                }
            }
        }
    }
    
    private func sendBorderCountriesRequest(_ country: Country) {
        var listOfCodes = country.borders?.map({ "\($0);" }).joined()
        if let listOfCodesSize = listOfCodes?.count, listOfCodesSize > 0 {
            listOfCodes?.removeLast()
        }
        if let listOfCodes = listOfCodes, listOfCodes.count > 0 {
            let urlString = "\(NetworkManager.baseUrl)\(NetworkManager.suffixBorderCountries)\(listOfCodes)"
            if let url = URL(string: urlString) {
                NetworkManager.fetchData(url: url) { [weak self] result in
                    switch result {
                    case .success(let data):
                        do {
                            let value = try JSONDecoder().decode([Country].self, from: data)
                            if value.count > 0 {
                                self?.countriesModel?.value = Countries(countries: value)
                            }else{
                                self?.countriesModel?.value = Countries(countries: value)
                                print("No Countries Received")
                            }
                        } catch {
                            self?.countriesModel?.value = Countries(countries: [])
                            print(error.localizedDescription)
                        }
                    case .failure:
                        self?.countriesModel?.value = Countries(countries: [])
                        print("Fail To Fetch Data")
                    }
                }
            }
            
        }
        else {
            print("No Borders Codes")
        }
    }
    
    private func sortByName() {
        if sortTypeDirection[.name] == .down {
            countriesModel?.value?.countries?.sort(by: { ($0.name ?? "") > ($1.name ?? "") })
            sortTypeDirection[.name] = .up
        }
        else {
            countriesModel?.value?.countries?.sort(by: { ($0.name ?? "") < ($1.name ?? "") })
            sortTypeDirection[.name] = .down
        }
    }
    
    private func sortByArea() {
        if sortTypeDirection[.area] == .down {
            countriesModel?.value?.countries?.sort(by: { ($0.area ?? -1) > ($1.area ?? -1) })
            sortTypeDirection[.area] = .up
        }
        else {
            countriesModel?.value?.countries?.sort(by: { ($0.area ?? -1) < ($1.area ?? -1) })
            sortTypeDirection[.area] = .down
        }
    }
}
