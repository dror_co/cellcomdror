//
//  CountryTableViewCell.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell, CellIdentifiable {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    var countryCellViewModel: CountryCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initViews()
    }
    
    private func initViews() {
        topLabel.text = ""
        bottomLabel.text = ""
    }
    
    func configure(country: Country) {
        countryCellViewModel = CountryCellViewModel(country: country)
        countryCellViewModel?.countryCellModel.bindAndFire({ country in
            self.topLabel.text = country?.name
            self.bottomLabel.text = country?.nativeName
        })
    }
}
