#### Countries app for Cellcom
#### Dror Cohen

Launch screen

![No Hairline](https://bitbucket.org/dror_co/cellcomdror/raw/986050c1d1ebdd8efce7cc240a6c188a6f064594/Images/3.png)

First screen - all countries 

![No Hairline](https://bitbucket.org/dror_co/cellcomdror/raw/63f8b9ff01bfc27a09c4599f7487108731a84c7b/Images/1.png)

Second screen - border countries

![No Hairline](https://bitbucket.org/dror_co/cellcomdror/raw/2374ad708b656b67e6ecfc3a7164b887f5e3ce86/Images/2.png)
