//
//  Country.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

struct Country: Codable {
    let nativeName : String?
    let name : String?
    let area : Double?
    let borders : [String]?

    enum CodingKeys: String, CodingKey {
        case nativeName = "nativeName"
        case name = "name"
        case area = "area"
        case borders = "borders"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        nativeName = try values.decodeIfPresent(String.self, forKey: .nativeName)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        area = try values.decodeIfPresent(Double.self, forKey: .area)
        borders = try values.decodeIfPresent([String].self, forKey: .borders)
    }
}
