//
//  CountryCellViewModel.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 01/05/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

class CountryCellViewModel {
    
    var countryCellModel: Observable<Country?>
    
    init(country: Country) {
        countryCellModel = Observable(country)
    }
}
