//
//  CountriesStep2ViewController.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import UIKit

class CountriesStep2ViewController: UIViewController, Loadable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var loadingView: UIActivityIndicatorView?
    var countriesViewModel: CountriesViewModel?
    var selectedCountry: Country?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addLoadingView()
        initTableView()
        initViewModel()
    }
    
    private func initViewModel() {
        showLoading()
        countriesViewModel = CountriesViewModel(type: .single, country: selectedCountry)
        countriesViewModel?.countriesModel?.bind({ [weak self] countries in
            self?.reloadTableView()
        })
    }
    
    private func initTableView() {
        tableView.register(CountryTableViewCell.cellNib, forCellReuseIdentifier: CountryTableViewCell.cellID)
    }

    private func reloadTableView() {
        tableView.dataSource = self
        tableView.reloadData()
        hideLoading()
    }
}

extension CountriesStep2ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesViewModel?.countriesModel?.value?.countries?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let countryTableViewCell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.cellID, for: indexPath) as? CountryTableViewCell, let country = countriesViewModel?.countriesModel?.value?.countries?[indexPath.row] {
            
            countryTableViewCell.configure(country: country)
            return countryTableViewCell
        }
        return UITableViewCell()
    }
}
