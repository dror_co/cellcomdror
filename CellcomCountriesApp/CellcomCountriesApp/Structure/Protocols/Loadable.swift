//
//  Loadable.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import UIKit

protocol Loadable: class {
    var loadingView: UIActivityIndicatorView? { get set }
    
    func addLoadingView()
    func showLoading()
    func hideLoading()
}

extension Loadable where Self: UIViewController {
    func addLoadingView() {
        let loadingView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        loadingView.color = .black
        loadingView.backgroundColor = .white
        self.loadingView = loadingView
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loadingView)
        
        let centerHorizontalConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let centerVerticalConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        view.addConstraint(centerHorizontalConstraint)
        view.addConstraint(centerVerticalConstraint)
    }
    
    func showLoading() {
        loadingView?.isHidden = false
        loadingView?.startAnimating()
    }
    
    func hideLoading() {
        loadingView?.stopAnimating()
        loadingView?.isHidden = true
    }
}
