//
//  CountriesStep1ViewController.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 30/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import UIKit

class CountriesStep1ViewController: UIViewController, Loadable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var loadingView: UIActivityIndicatorView?
    var countriesViewModel: CountriesViewModel?
    var selectedCountryCellViewModel: CountryCellViewModel?
    let borderCountriesID = "moveToBorderCountries"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addLoadingView()
        initTableView()
        initViewModel()
    }
    
    private func initViewModel() {
        showLoading()
        countriesViewModel = CountriesViewModel(type: .all)
        countriesViewModel?.countriesModel?.bind({ [weak self] countries in
            self?.reloadTableView()
        })
    }
    
    private func initTableView() {
        tableView.register(CountryTableViewCell.cellNib, forCellReuseIdentifier: CountryTableViewCell.cellID)
    }

    private func reloadTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        hideLoading()
    }
    
    @IBAction func sortByNameButtonPressed(_ sender: UIBarButtonItem) {
        countriesViewModel?.navigationBarLeftItemPressed()
    }
    
    @IBAction func sortByAreaButtonPressed(_ sender: UIBarButtonItem) {
        countriesViewModel?.navigationBarRightItemPressed()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextVC = segue.destination as? CountriesStep2ViewController {
            nextVC.selectedCountry = selectedCountryCellViewModel?.countryCellModel.value
        }
    }
}

extension CountriesStep1ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? CountryTableViewCell, let country = selectedCell.countryCellViewModel?.countryCellModel.value {
            if let bordersSize = country.borders?.count, bordersSize > 0 {
                self.selectedCountryCellViewModel = selectedCell.countryCellViewModel
                performSegue(withIdentifier: borderCountriesID, sender: nil)
            }
            else {
                // show alert - no borders
            }
        }
    }
}

extension CountriesStep1ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesViewModel?.countriesModel?.value?.countries?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let countryTableViewCell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.cellID, for: indexPath) as? CountryTableViewCell, let country = countriesViewModel?.countriesModel?.value?.countries?[indexPath.row] {
            
            countryTableViewCell.configure(country: country)
            return countryTableViewCell
        }
        return UITableViewCell()
    }
}
