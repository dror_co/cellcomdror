//
//  NetworkError.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 29/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation

enum NetworkError: String, Error {
    case parseFailed = "parse failed"
    case conditionFailed = "condition failed"
    case noNetwork = "no network"
    case unknownError = "unknown error"
}
