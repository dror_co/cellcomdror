//
//  NetworkManager.swift
//  CellcomCountriesApp
//
//  Created by dror cohen on 28/04/2021.
//  Copyright © 2021 dror cohen. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    static let baseUrl: String = "https://restcountries.eu/rest/v2"
    static let suffixAllCountries: String = "/all"
    static let suffixBorderCountries: String = "/alpha?codes="
    
    static var isReachableNetwork: Bool {
        return NetworkReachabilityManager.default?.isReachable ?? false
    }
    
    static func fetchData(url: URL, timeout: TimeInterval = 10, completion: @escaping(Result<Data, Error>) -> Void) {
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = timeout
        if isReachableNetwork {
            AF.request(urlRequest).responseData { response in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        completion(.success(data))
                    } else {
                        completion(.failure(NetworkError.parseFailed))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
        else {
            completion(.failure(NetworkError.noNetwork))
        }
    }
}
